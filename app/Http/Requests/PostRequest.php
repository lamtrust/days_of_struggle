<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required' , 'string'],
            'body' => ['required' , 'string']
        ];
    }

    public function messages(){
        return [
            'title.string' => 'You are a fool',
            'body.string' => 'a string also'
        ];
    }
}
