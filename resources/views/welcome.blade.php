@extends('layout.layout')
@section('navigationbar')
     @include('layout.navigation')
@endsection
@section('contents')
    <div class="container-fluid">
        <form action="{{ route('post.store') }}" method="POST">
           @csrf
            <div class="row">
                <div class="col-md-6">
                    <label for="title">Title</label>
                    <div class="form-group">
                        <input type="text" name="title" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="body">Body</label>
                    <div class="form-group">
                        <input type="text" name="body" class="form-control">
                    </div>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Submit</button>
        </form>
    </div>

    <div class="row">
        <table>
            <thead>
             <tr>
                 <th>title</th>
                 <th>body</th>
             </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
                <tr>
                    <th>{{ $post->title }}</th>
                    <th>{{ $post->body }}</th>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
@section('modals')
@endsection
@section('pagejs')
@endsection
